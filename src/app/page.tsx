import {ImageSkeletonContainer} from "@/components/Skeletons/Skeletons";
import Hero from "@/components/Home/Hero/Hero";


export default async function Home(){
      return (
        <main className='relative'>
            <Hero />
        </main>
      );
}
